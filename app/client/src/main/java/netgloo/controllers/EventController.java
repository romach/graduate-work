package netgloo.controllers;

import netgloo.models.Event;
import netgloo.models.EventDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EventController {

    @Autowired
    private EventDao eventDao;

    @RequestMapping("/")
    public String index() {
        return "search";
    }

    @RequestMapping(value = "/create")
    public String create(String url, String title, String address, String date) {
        try {
            Event event = new Event(url, title, address, date);
            eventDao.create(event);
        } catch (Exception ex) {
            return "Error creating the event: " + ex.toString();
        }
        return "created";
    }

    @RequestMapping(value = "/delete")
    public String delete(long id) {
        try {
            Event event = new Event(id);
            eventDao.delete(event);
        } catch (Exception ex) {
            return "Error deleting the event: " + ex.toString();
        }
        return "Event succesfully deleted!";
    }

    @RequestMapping(value = "/get-all")
    public String getAll(Model model) {
        List<Event> events = new ArrayList<>();
        events = eventDao.getAll();
        model.addAttribute("events", events);
        return "search";
    }

    @RequestMapping(value = "/add-test-events")
    public String addTestEvents() {
        List<Event> events = new ArrayList<>();
        events.add(new Event("http://www.meetup.com/virtualJUG/events/230866327/", "JUG24 - A 24 hour Virtual Java Conference", "Needs a location", "Tue Sep 27 12:00 AM"));
        events.add(new Event("http://www.meetup.com/Londonjavacommunity/events/231442891/", "48HR RAFFLE: 1 Tickets for BCS SPA conference June 27-29th, London", "", "Mon May 30 9:00 PM"));
        for (int i = 0; i < events.size(); i++) {
            eventDao.create(events.get(i));
        }
        return "search";
    }

    @RequestMapping(value = "/search")
    public String search(String query, Model model) {
        List<Event> events = new ArrayList<>();
        events = eventDao.getByQuery(query);
        model.addAttribute("events", events);
        return "search";
    }

}