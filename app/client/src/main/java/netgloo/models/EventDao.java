package netgloo.models;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class EventDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(Event event) {
        entityManager.persist(event);
        return;
    }

    public void delete(Event event) {
        if (entityManager.contains(event))
            entityManager.remove(event);
        else
            entityManager.remove(entityManager.merge(event));
        return;
    }

    @SuppressWarnings("unchecked")
    public List<Event> getAll() {
        return entityManager.createQuery("from Event").getResultList();
    }

    public Event getById(long id) {
        return entityManager.find(Event.class, id);
    }

    public void update(Event event) {
        entityManager.merge(event);
        return;
    }

    @SuppressWarnings("unchecked")
    public List<Event> getByQuery(String query) {
        return entityManager.createQuery("from Event e WHERE e.title LIKE :query")
            .setParameter("query", "%" + query + "%")
            .getResultList();
       // return null;
    }
}
